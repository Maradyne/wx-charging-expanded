name = "WX Charging Expanded"
author = "Maradyne"
version = "1.1"
description =
[[WX receives...
- A small charge when taking electrical damage.
- A medium charge when eating certain foods.
- A large charge when touching charged Lightning Rods or Generators.

Certain examination quotes/announcements have been changed/added to match.


Also adds one silly new way to die.]]



api_version = 10

dst_compatible = true
dont_starve_compatible = false

all_clients_require_mod = true
client_only_mod = false

server_filter_tags = {
	"Maradyne",
	"WX",
	"Charge"
}

icon_atlas = "modicon.xml"
icon = "modicon.tex"





--Tasty ConFigs--
-- formatting codes yoinked from Uncompromising Mode lol thanks fam
local function BinaryConfig(name, label, hover, default)
    return { name = name, label = label, hover = hover, options = { {description = "Enabled", data = true}, {description = "Disabled", data = false}, }, default = default, }
end

configuration_options = {
	BinaryConfig("WX_PvP_Bypass", "WX PvP Bypass", "Electrical damage toward WX bypasses PvP. Allows aggressive charging.", true)
}
