local require = GLOBAL.require

--External Scripts--
modimport("scripts/speech_wx78.lua")
modimport("scripts/strings_announce_mara_nocharge.lua")
modimport("postinit/electrical_attacks.lua")
modimport("postinit/electrical_food.lua")
modimport("postinit/electrical_weapons.lua")
modimport("postinit/lightning_rod.lua")
modimport("postinit/winona_batteries.lua")
modimport("postinit/wx78.lua")

if GetModConfigData("WX_PvP_Bypass") == true and not GLOBAL.TheNet:GetPVPEnabled() then
	modimport("scripts/components/combat_wx_electricution.lua")
end





--Internal Scripts--
-- nope





--       __________        __________
--      / ________ \      / ________ \
--     / /        \ \    / /        \ \
--    / /          \ \  / /          \ \
--   / /            \ \/ /            \ \
--  / /   Maradyne   \  /              \ \
--  \ \     \o/      /  \              / /
--   \ \            / /\ \            / /
--    \ \          / /  \ \          / /
--     \ \________/ /    \ \________/ /
--      \__________/      \__________/
-- 