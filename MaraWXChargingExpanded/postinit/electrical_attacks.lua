local env = env
GLOBAL.setfenv(1, GLOBAL)



-- couwd yuu make dis INSTRUMENT OF SLAUGHTER spawky plis?
local function MakeEquippedWeaponElectricalPls(inst)
	local handitem = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)

	if inst.components.inventory ~= nil
	and handitem ~= nil
	and handitem.components.weapon ~= nil
	then
		-- Electrical attacks multiply damage even without wetness, so we cut the damage accordingly.
		handitem.components.weapon:SetDamage(inst.components.combat.defaultdamage*.67)
		handitem.components.weapon:SetElectric()
	end
end


--Bishop--
env.AddPrefabPostInit("bishop", function(inst)
	if not TheWorld.ismastersim then
		return
	end
	
	inst:DoTaskInTime(0,MakeEquippedWeaponElectricalPls)
end)

--Damaged Bishop--
env.AddPrefabPostInit("bishop_nightmare", function(inst)
	if not TheWorld.ismastersim then
		return
	end
	
	inst:DoTaskInTime(0,MakeEquippedWeaponElectricalPls)
end)
