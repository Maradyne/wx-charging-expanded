local env = env
GLOBAL.setfenv(1, GLOBAL)


--Electrical Foods--
env.AddPrefabPostInit("goatmilk", function(inst)
	if not TheWorld.ismastersim then
		return
	end
	
	inst:AddTag("mara_electrofood")
end)

env.AddPrefabPostInit("voltgoatjelly", function(inst)
	if not TheWorld.ismastersim then
		return
	end
	
	inst:AddTag("mara_electrofood")
	-- Extra tag to inform client-side that this food gives the electricattacks buff.
	-- ...because vanilla doesn't think that's important.
	inst:AddTag("electricattacks_giver")
end)
