local env = env
GLOBAL.setfenv(1, GLOBAL)


-- Vanilla electrical weapons don't have a unifying tag.
-- Additionally, the electrical stimuli isn't set until after telling the client to stop reading.
-- ...so, there's no way for the client to know if a weapon is electrical; it only hears the hit result.
-- So if we want to do anything before attacking, like allowing an attack to happen in the first place...
-- Then every electrical weapon needs a tag. Unfortunately.

-- The tag "electricalweapon" identifies weapons that can bypass PvP toward WX (when the config is enabled).
-- You can add it to your equippables to automatically tap into that functionality.

--Electric Dart--
env.AddPrefabPostInit("blowdart_yellow", function(inst)
	inst:AddTag("electricalweapon")
end)

--Morning Star--
env.AddPrefabPostInit("nightstick", function(inst)
	inst:AddTag("electricalweapon")
end)
