local env = env
GLOBAL.setfenv(1, GLOBAL)


--Cloned Lightning Rod Functions--
local ondaycomplete

local function discharge(inst)
    if inst.charged then
        inst:StopWatchingWorldState("cycles", ondaycomplete)
        inst.AnimState:ClearBloomEffectHandle()
        inst.charged = false
        inst.chargeleft = nil
        inst.Light:Enable(false)
        if inst.zaptask ~= nil then
            inst.zaptask:Cancel()
            inst.zaptask = nil
        end
    end
end


--New Stuff--
local function GetActivateVerb()
    return "TOUCH"
end

local function OnActivate(inst, doer)
	if inst.charged == nil or inst.charged ~= nil and inst.chargeleft == nil or inst.chargeleft <= 0 then
		doer.components.talker:Say(GetString(doer, "ANNOUNCE_MARA_NOCHARGE"))
	end
	
	if doer.components.combat ~= nil and (doer.components.inventory == nil or not doer.components.inventory:IsInsulated()) then
		if inst.charged ~= nil and inst.chargeleft ~= nil and inst.chargeleft > 0 then
			inst.chargeleft = inst.chargeleft - 3
			discharge(inst)
			doer.components.combat:GetAttacked(inst, TUNING.LIGHTNING_DAMAGE, nil, "electric")
		end
	end
	inst.components.activatable.inactive = true
end


env.AddPrefabPostInit("lightning_rod", function(inst)
    inst.GetActivateVerb = GetActivateVerb
	
    inst.entity:SetPristine()
	if not TheWorld.ismastersim then
		return
	end
	
	
	inst:AddComponent("activatable")
	inst.components.activatable.OnActivate = OnActivate
	inst.components.activatable.quickaction = true
	inst.components.activatable.inactive = true
end)
