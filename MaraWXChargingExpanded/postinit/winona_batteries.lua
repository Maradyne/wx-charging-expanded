local env = env
GLOBAL.setfenv(1, GLOBAL)


--New Stuff--
local function GetActivateVerb()
    return "TOUCH"
end

local function OnActivate(inst, doer)
	if inst.components.fueled.currentfuel <= 0 then
		doer.components.talker:Say(GetString(doer, "ANNOUNCE_MARA_NOCHARGE"))
	end
	
	if doer.components.combat ~= nil and (doer.components.inventory == nil or not doer.components.inventory:IsInsulated()) then
		if inst.components.fueled.currentfuel > 0 then
			inst.components.fueled.currentfuel = inst.components.fueled.currentfuel - 90
			doer.components.combat:GetAttacked(inst, TUNING.LIGHTNING_DAMAGE, nil, "electric")
			
			-- Check if the generator's fuel value has been made negative by the player zap. Then...
			-- We make the fuel value barely positive, so it can dwindle and naturally unpower the generator.
			-- ...without having to clone a bunch of functions and make a mess of mod compatibility.
			-- We want a small enough value that people can't sustain the charge with rapid activation.
			if inst.components.fueled.currentfuel <= 0 then
				inst.components.fueled.currentfuel = .0000000001
			end
		end
	end
	inst.components.activatable.inactive = true
end


env.AddPrefabPostInit("winona_battery_low", function(inst)
    inst.GetActivateVerb = GetActivateVerb
	
    inst.entity:SetPristine()
	if not TheWorld.ismastersim then
		return
	end
	
	
	inst:AddComponent("activatable")
	inst.components.activatable.OnActivate = OnActivate
	inst.components.activatable.quickaction = true
	inst.components.activatable.inactive = true
end)

env.AddPrefabPostInit("winona_battery_high", function(inst)
    inst.GetActivateVerb = GetActivateVerb
	
    inst.entity:SetPristine()
	if not TheWorld.ismastersim then
		return
	end
	
	
	inst:AddComponent("activatable")
	inst.components.activatable.OnActivate = OnActivate
	inst.components.activatable.quickaction = true
	inst.components.activatable.inactive = true
end)