--local ModEnabledUncomp = GLOBAL.KnownModIndex:IsModEnabled("workshop-2039181790")

local env = env
GLOBAL.setfenv(1, GLOBAL)


--Cloned WX Functions--
local function onupdate(inst, dt)
    inst.charge_time = inst.charge_time - dt
    if inst.charge_time <= 0 then
        inst.charge_time = 0
        if inst.charged_task ~= nil then
            inst.charged_task:Cancel()
            inst.charged_task = nil
        end
        inst.SoundEmitter:KillSound("overcharge_sound")
        inst:RemoveTag("overcharge")
        inst.Light:Enable(false)
        inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED
        inst.components.bloomer:PopBloom("overcharge")
        inst.components.temperature.mintemp = -20
        inst.components.talker:Say(GetString(inst, "ANNOUNCE_DISCHARGE"))
    else
        local runspeed_bonus = .5
        local rad = 3
        if inst.charge_time < 60 then
            rad = math.max(.1, rad * (inst.charge_time / 60))
            runspeed_bonus = (inst.charge_time / 60)*runspeed_bonus
        end

        inst.Light:Enable(true)
        inst.Light:SetRadius(rad)
        --V2C: setting .runspeed does not stack with mount speed
        inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED*(1+runspeed_bonus)
        inst.components.temperature.mintemp = 10
    end
end

local function startovercharge(inst, duration)
    inst.charge_time = duration

    inst:AddTag("overcharge")
    inst:PushEvent("ms_overcharge")

    inst.SoundEmitter:KillSound("overcharge_sound")
    inst.SoundEmitter:PlaySound("dontstarve/characters/wx78/charged", "overcharge_sound")
    inst.components.bloomer:PushBloom("overcharge", "shaders/anim.ksh", 50)

    if inst.charged_task == nil then
        inst.charged_task = inst:DoPeriodicTask(1, onupdate, nil, 1)
        onupdate(inst, 0)
    end
end





--New Stuff--
-- This induces a small charge.
local function mara_wxcharge_small(inst)
	if inst.components.health ~= nil and not (inst.components.health:IsDead() or inst.components.health:IsInvincible()) then
		if not inst.components.inventory:IsInsulated() then
			if 
				not inst:HasTag("automaton") 
				--not ModEnabledUncomp 
			then
				inst.components.health:DoDelta(TUNING.HEALING_MED, false, "lightning")
			end
			inst.components.sanity:DoDelta(-TUNING.SANITY_TINY)
			inst.components.talker:Say(GetString(inst, "ANNOUNCE_CHARGE"))
			
			startovercharge(inst, CalcDiminishingReturns(inst.charge_time, TUNING.SEG_TIME))
		end
	end
end

-- This induces a medium charge.
local function mara_wxcharge_medium(inst)
	if inst.components.health ~= nil and not (inst.components.health:IsDead() or inst.components.health:IsInvincible()) then
		if not inst.components.inventory:IsInsulated() then
			if 
				not inst:HasTag("automaton") 
				--not ModEnabledUncomp 
			then
				inst.components.health:DoDelta(TUNING.HEALING_LARGE, false, "lightning")
			end
			inst.components.sanity:DoDelta(-TUNING.SANITY_MED)
			inst.components.talker:Say(GetString(inst, "ANNOUNCE_CHARGE"))
			
			startovercharge(inst, CalcDiminishingReturns(inst.charge_time, TUNING.SEG_TIME*6))
		end
	end
end

-- This induces a large charge, equal to a lightning strike.
local function mara_wxcharge_large(inst)
	if inst.components.health ~= nil and not (inst.components.health:IsDead() or inst.components.health:IsInvincible()) then
		if not inst.components.inventory:IsInsulated() then
			if 
				not inst:HasTag("automaton") 
				--not ModEnabledUncomp 
			then
				inst.components.health:DoDelta(TUNING.HEALING_SUPERHUGE, false, "lightning")
			end
			inst.components.sanity:DoDelta(-TUNING.SANITY_LARGE)
			inst.components.talker:Say(GetString(inst, "ANNOUNCE_CHARGE"))
			
			startovercharge(inst, CalcDiminishingReturns(inst.charge_time, TUNING.TOTAL_DAY_TIME))
		end
	end
end


-- When any electrical attack hits us, give a small charge.
-- Or a large charge, in certain cases.
local function OnAttacked(inst, data)
	if data ~= nil and data.attacker ~= nil then
		if data.attacker.prefab == "lightning_rod"
		or data.attacker.prefab == "winona_battery_low"
		or data.attacker.prefab == "winona_battery_high"
		then
			inst:DoTaskInTime(0.2, mara_wxcharge_large)
		elseif data.stimuli == "electric" or (data.weapon ~= nil and data.weapon.components.weapon ~= nil and data.weapon.components.weapon.stimuli == "electric") then
			inst:DoTaskInTime(0.2, mara_wxcharge_small)
		end
	end
end

-- Anything tagged with mara_electrofood will give WX a medium charge upon consumption.
local function mara_electrofood_check(inst, data)
	if data ~= nil and data.food ~= nil then
		if data.food.components.edible and data.food:HasTag("mara_electrofood") then
			inst:DoTaskInTime(0.2, mara_wxcharge_medium)
		end
	end
end





--WX Function Activators--
env.AddPrefabPostInit("wx78", function(inst)
	if not TheWorld.ismastersim then
		return
	end
	
	inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("oneat", mara_electrofood_check)
--	inst.components.eater:SetOnEatFn(mara_electrofood_check) -- Overwrites gear upgrades; here for reference.
end)
