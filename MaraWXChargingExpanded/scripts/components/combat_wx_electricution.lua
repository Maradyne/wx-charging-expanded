-- Gives people buffed by food that makes attacks electrical a tag.
-- This lets us detect the buff client-side, so we can use it to approve combat targets. 
local function electricattacks_tagger(inst, data)
	if data ~= nil and data.food ~= nil then
		if
			data.food.components.edible
			and data.food:HasTag("electricattacks_giver")
		then
			inst:AddTag("electricattacksenabled")
			inst:DoTaskInTime(TUNING.BUFF_ELECTRICATTACK_DURATION, function()
				inst:RemoveTag("electricattacksenabled")
			end)
		end
	end
end

AddPlayerPostInit(function(inst)
	inst:ListenForEvent("oneat", electricattacks_tagger)
end)


-- We copy this from componentutil.lua because it just...refuses to load from there. Must be shy.
-- It tells us if objects are in the middle of dying.
local function IsEntityDead(inst, require_health)
	if inst.replica.health == nil then
		return require_health == true
	end
	return inst.replica.health:IsDead()
end


-- Functions to check if the target is WX, and if the attacker is using electricity.
-- So we can charge him via PvP without having PvP turned on.

-- This functions pretty cleanly; we're just adding more cases that can return 'true' to the victim.
-- Only triggers after the victim has already been successfully attacked.
local function MaraWXElectricalCanBeAttacked(self, attacker)
	if
		(self.inst.prefab == "wx78" and attacker:HasTag("player"))
		and not (
			self.inst:HasTag("playerghost")
			or self.inst:HasTag("noattack")
			or self.inst:HasTag("invisible")
		)
	then
		local combat = attacker.replica.combat
		local weapon = combat ~= nil and combat:GetWeapon() or nil
		if 
			attacker:HasTag("electricattacksenabled")
			-- I've tried so many other ways to do this, but tagging the weapon is the only one that worked.
			-- Because targeting has to take place client-side, which lacks a LOT of information.
			-- The problem with Electric Darts is that by the time the last projectile in the stack hits...
			-- The 'weapon' with the tag that launched it no longer exists.
			or (weapon ~= nil and weapon:HasTag("electricalweapon"))
		then
			-- If everything checks out, bring on the pain.
			return true
		end
	end
end





--Class Edit--
AddClassPostConstruct("components/combat_replica", function(self)
	-- Here, whenever Combat:IsValidTarget inside the old component runs, we intercept it.
	-- Then we use that as a trigger to run our version if target is WX; otherwise, run the old one.	
	local vanillaIsValidTarget = self.IsValidTarget
	
	function self:IsValidTarget(target)
		--print "self:IsValidTarget is triggering properly."
		
		-- If the target is ourself or basically doesn't exist, just stop here.
		if
			target == nil
			or target == self.inst
			or not (
				target.entity:IsValid()
				and target.entity:IsVisible()
			)
		then
			return false
		end
		
		
		-- Make sure this stays up to date with player-related code in Combat:IsValidTarget.
		-- Even if some parts aren't actually used in vanilla, copy them in case mods use them.
		-- Server PvP setting is checked in modmain; no need to check here.
		local attackerweapon = self:GetWeapon()
		
		if self.inst:HasTag("player")
			and (target ~= nil and target.prefab == "wx78")
			and (
				self.inst:HasTag("electricattacksenabled")
				or (attackerweapon ~= nil and attackerweapon:HasTag("electricalweapon"))
			)
		then
			return (
				target.replica.combat ~= nil
				and not target:HasTag("spawnprotection")
				and not IsEntityDead(target, true)
				and not (
					target:HasTag("playerghost")
					and (
						self.inst.replica.sanity == nil
						or self.inst.replica.sanity:IsSane()
					)
					and not self.inst:HasTag("crazy")
				)
				and target:GetPosition().y <= self._attackrange:value()
			)
		else
			return vanillaIsValidTarget(self, target)
		end
	end
	
	
	-- Here, whenever Combat:CanBeAttacked inside the old component runs, we intercept it.
	-- We use that as a trigger to return true if either the old component or our new one returns true.
	local vanillaCanBeAttacked = self.CanBeAttacked
	
	function self:CanBeAttacked(attacker)
		if
			vanillaCanBeAttacked(self, attacker) or MaraWXElectricalCanBeAttacked(self, attacker)
		then
			return true
		else
			return false
		end
	end
end)





-- sweet baby satan release me from this fresh hell
